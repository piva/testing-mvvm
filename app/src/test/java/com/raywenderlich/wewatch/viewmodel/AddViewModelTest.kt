package com.raywenderlich.wewatch.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.raywenderlich.wewatch.data.MovieRepository
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AddViewModelTest {
    private lateinit var addViewModel: AddViewModel

    @Mock
    lateinit var repository: MovieRepository

    @Before
    fun setUp(){
        addViewModel = AddViewModel(repository)
    }

    /**
     * java.lang.RuntimeException: Method getMainLooper in android.os.Looper not mocked
     * LiveData .postValue() is executed async and unit test run
     * on the main thread it is Necessary to run all tests on main thread and solve
     */
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun canSaveMovieWuthoutTitle(){
        addViewModel.title.set("")
        addViewModel.releaseDate.set("")

        val canSaveMovie = addViewModel.canSaveMovie()
        assertEquals(false, canSaveMovie)
    }

    @Test
    fun canSaveMovieWithoutDate(){
        addViewModel.title.set("myMovie")
        addViewModel.releaseDate.set("")

        val canSaveMovie = addViewModel.canSaveMovie()
        assertEquals(false, canSaveMovie)
    }

    @Test
    fun isProperlySaved(){
        addViewModel.title.set("Titanic")
        addViewModel.releaseDate.set("1998")
        addViewModel.saveMovie()
        assertEquals(true, addViewModel.getSaveLiveData().value)
    }


}